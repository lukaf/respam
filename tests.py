import unittest
import respam
import redis
import random


class TestRespam(unittest.TestCase):
    def setUp(self):
        self.r = redis.StrictRedis(host='192.168.71.1')
        self.hosts = self.r.smembers('spamstat:hosts')

    def test_types(self):
        self.assertTrue(isinstance(self.hosts, set))
        # spammers returns set
        self.assertTrue(isinstance(respam.spammers(self.hosts, self.r), set))
        # false_positives returns list
        self.assertTrue(isinstance(respam.false_positives(self.hosts, self.r),
            list))

    def test_false_positives(self):
        # should not fail if the hosts set is empty
        self.assertTrue(isinstance(respam.false_positives(set(), self.r),
            list))

    def test_spammers(self):
        # should not fail if the hosts set is empty
        self.assertTrue(isinstance(respam.spammers(set(), self.r), set))

    def test_delete(self):
        # should not fail if the hosts is empty
        self.assertEqual(respam.delete([], self.r), 0)
        # deleting hosts results in:
        # decreased spamstat:counter
        # removed spam:<ip>
        # hosts removed from spamstat:hosts
        host = random.sample(self.hosts, 1)
        # is it a list?
        self.assertTrue(isinstance(host, list))
        # does it really exist?
        self.assertTrue(self.r.sismember('spamstat:hosts', host[0]))
        count = self.r.get('spamstat:counter')
        # is it a number?
        self.assertTrue(isinstance(int(count), int))
        # deletion should return True
        self.assertTrue(respam.delete(host, self.r))
        self.assertTrue(int(count) > int(self.r.get('spamstat:counter')))
        self.assertEqual(self.r.hgetall('spam:%s' % host[0]), {})
        self.assertFalse(self.r.sismember('spamstat:hosts', host[0]))
        # should return number of hosts
        host_many = random.sample(
            self.hosts, random.randint(1, len(self.hosts))
        )
        self.assertEqual(respam.delete(host_many, self.r), len(host_many))


if __name__ == '__main__':
    unittest.main()
