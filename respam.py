import datetime
import itertools

time_fmt = '%Y%m%d%H%M'
now = datetime.datetime.now()


def delete(hosts, r):
    if len(hosts) > 0:
        pipe = r.pipeline()
        for host in hosts:
            pipe.delete('spam:%s' % host)
            pipe.decr('spamstat:counter')
            pipe.srem('spamstat:hosts', host)
        pipe.execute()
    return len(hosts)


# return spammers
def spammers(hosts, r):
    spammers_l = set()
    pipe = r.pipeline()
    for host in hosts:
        pipe.hget('spam:%s' % host, 'count')
    host_count = pipe.execute()

    # even if hosts are in set, list returned by pipe is a match but only
    # if the list stays untouched!
    for host, count in itertools.izip(hosts, host_count):
        if int(count) > 10:
            spammers_l.add(host)
    return spammers_l


def false_positives(hosts, r):
    # remove all spammers
    non_spammers = []
    hosts = hosts - spammers(hosts, r)
    pipe = r.pipeline()
    for host in hosts:
        pipe.hget('spam:%s' % host, 'last_seen')
    last_seen = pipe.execute()

    # same as in spammers() case
    for host, timestamp in itertools.izip(hosts, last_seen):
        if (now - datetime.datetime.strptime(timestamp, time_fmt)).days > 60:
            non_spammers.append(host)
    return non_spammers
